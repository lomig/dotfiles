vim.g.mapleader='.'
vim.g.maplocalleader='.'

function map(mode, lhs, rhs, opts)
    local options = { noremap = true }
    if opts then
        options = vim.tbl_extend("force", options, opts)
    end
    vim.api.nvim_set_keymap(mode, lhs, rhs, options)
            end

map("n", "<Leader>n", ":NvimTreeToggle<CR>")
map("n", "<Tab>", ":bnext<CR>")
map("n", "<S-Tab>", ":bprevious<CR>")
map("n", "<Leader>v", "<C-W>v")
map("n", "<Leader>s", "<C-W>s")
map("n", "<Leader>z", ":ZenMode<CR>")

vim.keymap.set(
    "",
    "<Leader>l",
    require("lsp_lines").toggle,
    { desc = "Toggle lsp_lines" }
)
