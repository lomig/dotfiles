local lsp = require("lsp-zero")
lsp.preset("recommended")

lsp.ensure_installed({
    'bashls', 'tsserver', 'ltex', 'sumneko_lua', 'marksman', 'pyright'
})
lsp.nvim_workspace()
lsp.setup()
